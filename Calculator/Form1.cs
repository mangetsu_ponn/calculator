﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        string strInputNumber = "";
        string calcSymbol = "";
        double result;
        double nextNumber;

        public Form1()
        {
            InitializeComponent();
        }

        private void NumberInputButton_Click(object sender, EventArgs e)
        {
            Button inputNumButton = (Button)sender;
            strInputNumber += inputNumButton.Text;
            tb_display.Text = strInputNumber;
        }

        private void CalculateButton_Click(object sender, EventArgs e)
        {
            if(calcSymbol == "")
            {
                result = double.Parse(strInputNumber);
            }

            Button calcSymbolButton = (Button)sender;
            calcSymbol = calcSymbolButton.Text;

            strInputNumber = "";
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            tb_display.Text = "";
            strInputNumber = "";
        }

        private void equalButton_Click(object sender, EventArgs e)
        {
            nextNumber = double.Parse(strInputNumber);

            if (calcSymbol == "+")
            {
                result += nextNumber;
            }
            if (calcSymbol == "-")
            {
                result -= nextNumber;
            }
            if (calcSymbol == "×")
            {
                result *= nextNumber;
            }
            if (calcSymbol == "÷")
            {
                result /= nextNumber;
            }

            tb_display.Text = result.ToString();
            calcSymbol = "";
        }
    }
}
